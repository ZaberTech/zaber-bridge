![Zaber Logo](docs/img/zaber-logo.png)

# Zaber Bridge

Zaber Bridge is a multi-platform command-line utility that allows control of Zaber devices from other computers over the network.
The functionality is similar to Ethernet to Serial Port converters.
The utility exposes serial ports on the computer as TCP/IP ports making them accessible from the network.
It can run as a user program or as a system service (in the background).

You can use this utility to remotely talk to devices using [Zaber Console](https://www.zaber.com/zaber-software) and [Zaber Motion Library](https://www.zaber.com/zaber-software) which both support TCP.

## Download and Installation

* [Windows](docs/install/windows.md)
* [Linux](docs/install/linux.md) (also Raspberry PI)
* [Mac OS](docs/install/macos.md)

## Configuration

Before running the program, you need to configure the mapping between serial ports and TCP ports.
The ports are configured using sub-commands of the `config` command.
Display the help for configuration by running:

```bash
zaber-bridge config --help
```

The output tells us that port mapping is added by the sub-command `add`.
Let's display its help to see all the options.

```bash
zaber-bridge config add --help
```

Use the `--help` flag to get help for any command or sub-command.

In order to add a new serial port you need to know its name.
You can display serial ports currently available on your computer using this command:

```bash
zaber-bridge tools list

# Outputs:
# - COM2
# - COM3
```

In the following command we add mapping of serial port `COM2` to expose it to the network.
Make sure to replace `COM2` with your port's name.

```bash
zaber-bridge config add --serial-port COM2 --tcp-port 7055

# Outputs:
# +-------------+----------+----------------+
# | Serial Port | TCP Port | Baud Rate      |
# +-------------+----------+----------------+
# | COM2        |     7055 | (ASCII) 115200 |
# +-------------+----------+----------------+
# New mapping added.
```

The command adds serial port `COM2` and maps it to the TCP port `7055`.
(The TCP/IP port number is selected arbitrarily for this example; typically you can use any number from 1024 to 65535 provided your operating system will allow it and no other programs are using it.)
You will need the port number along with the IP Address or hostname of the computer to connect to it (see [here](#determining-your-hostname-and-ip-address)).
If you wish to add more serial ports be sure to assign them different TCP ports (e.g. `7056, 7057, ...`).

You may notice the `Baud Rate` column in the output of the command above.
The baud rate is determined by the Zaber protocol. Zaber currently supports two protocols: ASCII and Binary.
ASCII protocol is more contemporary and therefore default.
To map the serial port with Binary protocol add `--protocol binary` option to the command.
Specifying the protocol is barely a way of setting the baud rate and does not restrict you to using a particular protocol on the port.
Furthermore, there is an option `--baud-rate` that allows mapping the port with any specific baud rate.
If you wish to access the same serial port with a different protocol, you can map it multiple times with different settings.

Use the following commands to display and remove mappings respectively.

```bash
zaber-bridge config list
zaber-bridge config remove --serial-port COM2
```

The whole configuration is stored in a YAML file.
On Windows, the file is located in the folder with the program.
On Linux and Mac OS, the file is in `~/.zaber-bridge` for non-root users and in `/etc/zaber-bridge` when running as root.

## Running

After you have setup your mappings, run the utility using this command:

```bash
zaber-bridge run
```

The program reads the configuration and listens on the TCP ports. You can stop the program by pressing `Ctrl + C`.

When there is an incoming connection on a TCP port, the program attempts to open the serial port and starts forwarding the data.
Once the remote computer closes the connection, the program closes the serial port.
This implementation allows other programs on your computer to use the serial port when there are no connections from remote computers.
Keep in mind that if the serial port cannot be opened, the incoming connection is rejected.
Also, a new incoming connection in addition to the already existing connection replaces the old one, and the old one is closed.
You can monitor the activity of the program by watching its console output.

## Running a single mapping

It is possible to bypass the configuration system and quickly run a single port mapping. This is possible using the `single` command and some arguments. The arguments follow the same syntax as the `config add` command. For example, running just a single mapping between `COM2` and TCP/IP port `7055`:

```bash
zaber-bridge single --serial-port COM2 --tcp-port 7055

# Outputs:
# Starting...
# Started TCP 7055 <-> COM2
# ...
# Press Ctrl+C to stop...
```

## Running as a system service (in the background)

Sometimes it is useful to run the program on computer startup and in the background without user interaction.
The installation and usage differ depending on the operating system:

* [Windows](docs/service/windows.md)
* [Linux](docs/service/linux.md)
* [Mac OS](docs/service/macos.md)

## Determining your hostname and IP address

The hostname and local IP address(es) of your computer are displayed after the program starts.
You can also display them using following commands:

```bash
zaber-bridge tools hostname

# Outputs:
# Hostname: lab-desktop-12
```

```bash
zaber-bridge tools ip

# Outputs:
# Local IP addresses:
# 192.168.100.179 (Ethernet)
# 192.168.1.15 (Wi-Fi)
```

Keep in mind that your computer may have multiple IP addresses assigned to different network adapters.
To connect to your computer from a remote location, you need the IP address of the network reachable from the remote location.

## Changelog

You can read our changelog [here](CHANGELOG.md).

## Support

Need assistance with the application?
Follow instructions on [zaber.com](https://www.zaber.com/contact) to contact our Applications Engineering Team.

Have feedback or suggestions for features?
Contact our Software Team by creating an [Issue](https://gitlab.com/ZaberTech/zaber-bridge/issues).
