module gitlab.izaber.com/software-public/zaber-bridge

go 1.18

require (
	github.com/kardianos/service v1.0.0
	github.com/syohex/go-texttable v0.0.0-20140622065955-d721bde1381e
	github.com/zabertech/go-serial v0.0.0-20210201195853-2428148c5139
	gopkg.in/urfave/cli.v1 v1.20.0
	gopkg.in/yaml.v2 v2.2.2
)

require (
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/chavacava/garif v0.1.0 // indirect
	github.com/creack/goselect v0.1.2 // indirect
	github.com/fatih/color v1.15.0 // indirect
	github.com/fatih/structtag v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/mgechev/dots v0.0.0-20210922191527-e955255bf517 // indirect
	github.com/mgechev/revive v1.3.4 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.1.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/tools v0.13.0 // indirect
)
