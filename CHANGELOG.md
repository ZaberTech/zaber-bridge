[:arrow_left: Index](README.md)

# Zaber Bridge Changelog

## 2025-01-24, Version 1.0.1

* Minor dependency updates.
* Wider platform support.

## 2019-11-07, Version 1.0.0

* **Release**
