import { expect } from 'chai';

import { createTty0Tty, Tty0Tty } from './tty0tty';
import { writeYaml } from './yml';
import { CONFIG_FILE, TEST_PORT, tryConnect, itLinux64 } from './utils';
import { spawn, Bridge } from './run';
import { TcpClient } from './tcp';
import { Tty } from './tty';

describe('Forwarding', () => {
  let tty: Tty;
  let tty0tty: Tty0Tty;

  beforeEach(async () => {
    tty0tty = await createTty0Tty();

    tty = new Tty(tty0tty.tty2);
    await tty.open();
  });
  afterEach(async () => {
    await tty.close();
    await tty0tty.end();
  });

  describe('single connection', () => {
    let bridge: Bridge;

    beforeEach(async () => {
      await writeYaml(CONFIG_FILE, {
        mappings: [{
          serial_port: tty0tty.tty1,
          baud_rate: 115200,
          tcp_port: TEST_PORT,
        }],
      });

      bridge = spawn('run');
    });
    afterEach(async () => {
      await bridge.end();
    });

    itLinux64('communicates both ways', async () => {
      const client = new TcpClient();
      try {
        await tryConnect(client);

        await client.pushLine('hello1');
        expect(await tty.popLine()).to.eq('hello1');

        await tty.pushLine('hello2');
        expect(await client.popLine()).to.eq('hello2');

        await client.pushLine('hello3');
        expect(await tty.popLine()).to.eq('hello3');

        await tty.pushLine('hello4');
        expect(await client.popLine()).to.eq('hello4');
      } finally {
        await client.close();
      }
    });

    itLinux64('allows multiple connections one after another', async () => {
      for (let i = 0; i < 3; i++) {
        const client = new TcpClient();
        try {
          await tryConnect(client, { attempts: i === 0 ? null : 1 });

          await client.pushLine('hello1');
          expect(await tty.popLine()).to.eq('hello1');

          await tty.pushLine('hello2');
          expect(await client.popLine()).to.eq('hello2');
        } finally {
          await client.close();
        }
      }
    });

    itLinux64('disconnects the old connection when a new one is opened', async () => {
      const client1 = new TcpClient();
      await tryConnect(client1);

      await client1.pushLine('hello1');
      expect(await tty.popLine()).to.eq('hello1');

      await tty.pushLine('hello2');
      expect(await client1.popLine()).to.eq('hello2');

      const client2 = new TcpClient();

      await  Promise.all([
        client1.waitForClosing(),
        tryConnect(client2, { attempts: 1 }),
      ]);

      await client2.pushLine('hello1');
      expect(await tty.popLine()).to.eq('hello1');

      await tty.pushLine('hello2');
      expect(await client2.popLine()).to.eq('hello2');

    });
  });

  describe('single connection, non existing port', () => {
    let bridge: Bridge;

    beforeEach(async () => {
      await writeYaml(CONFIG_FILE, {
        mappings: [{
          serial_port: '/dev/not_existing',
          baud_rate: 115200,
          tcp_port: TEST_PORT,
        }],
      });

      bridge = spawn('run');
    });

    afterEach(async () => {
      await bridge.end();
    });

    itLinux64('closes connection when it cannot find the port', async () => {
      const client = new TcpClient();
      await tryConnect(client);
      await client.waitForClosing();
    });
  });
});
