import { expect } from 'chai';
import { run } from './run';

describe('Help', () => {
  it('displays help', async () => {
    const { stdout } = await run('--help');

    expect(stdout).to.contain('USAGE:');
  });
});
