import { promises as fs } from 'fs';
import path from 'path';
import os from 'os';
import { delay } from 'bluebird';

import { TcpClient } from './tcp';

export const CONFIG_FILE = path.join(__dirname, 'zaber-bridge-config.yml');
export const TEST_PORT = 6789;

export async function cleanConfig(): Promise<void> {
  try {
    await fs.unlink(CONFIG_FILE);
  } catch (err) { } // tslint:disable-line
}

export async function tryConnect(client: TcpClient, options: { attempts?: number; port?: number } = {}): Promise<void> {
  let lastError: any;

  for (let attempt = options.attempts || 10; attempt > 0; attempt--) {
    try {
      await client.connect('127.0.0.1', options.port || TEST_PORT);
      return;
    } catch (err) {
      lastError = err;

      if (err.code !== 'ECONNREFUSED') {
        break;
      }

      await delay(200);
    }
  }

  throw lastError;
}

export const itLinux64 = os.arch() === 'x64' && os.platform() === 'linux' ? it : xit;
