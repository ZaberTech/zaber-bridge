import childProcess from 'child_process';
import path from 'path';
import readline from 'readline';
import Bluebird from 'bluebird';

export class Tty0Tty {
  public tty1: string;
  public tty2: string;
  private process: childProcess.ChildProcessWithoutNullStreams;
  private closePromise: Bluebird.Resolver<{ code: number; signal: string; }>;

  public async start(): Promise<void> {
    const file = path.join(__dirname, '..', 'tools', 'tty0tty');
    this.process = childProcess.spawn('stdbuf', `-i0 -o0 -e0 ${file}`.split(' '), {
      stdio: ['ignore', 'pipe', 'ignore'],
    });

    try {
      await this.setup();
    } catch (err) {
      this.kill();
      throw err;
    }
  }

  public async end(): Promise<void> {
    this.kill();

    this.closePromise = Bluebird.defer();
    await this.closePromise;
  }

  private kill(): void {
    this.process.kill();
  }

  private async setup(): Promise<void> {
    this.process.once('close', (code, signal) => {
      if (this.closePromise) {
        this.closePromise.resolve({ code, signal });
        this.closePromise = null;
      }
    });

    const rl = readline.createInterface({ input: this.process.stdout });

    const firstLine = await new Promise<string>((resolve, reject) => {
      rl.once('line', resolve);
      this.process.once('error', reject);
    });

    rl.close();

    const terminals = firstLine.match(/^\(([^\)]*)\) <=> \(([^\)]*)\)$/);
    if (!terminals) {
      throw new Error(`Cannot parse output: ${firstLine}`);
    }

    this.tty1 = terminals[1];
    this.tty2 = terminals[2];
  }
}

export async function createTty0Tty(): Promise<Tty0Tty> {
  const instance = new Tty0Tty();
  await instance.start();
  return instance;
}
