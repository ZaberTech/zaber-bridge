import { expect } from 'chai';

import { createTty0Tty, Tty0Tty } from './tty0tty';
import { writeYaml } from './yml';
import { CONFIG_FILE, TEST_PORT, tryConnect, itLinux64 } from './utils';
import { spawn, Bridge } from './run';
import { TcpClient } from './tcp';
import { Tty } from './tty';

describe('Smoke test', () => {
  const FORWARDING_COUNT = 10;
  const EXCHANGE_COUNT = 1000;

  let tty0ttys: Tty0Tty[];
  let ttys: Tty[];

  let bridge: Bridge;

  beforeEach(async () => {
    tty0ttys = [];
    ttys = [];

    for (let i = 0; i < FORWARDING_COUNT; i++) {
      const tty0tty = await createTty0Tty();

      const tty = new Tty(tty0tty.tty2);
      await tty.open();

      tty0ttys.push(tty0tty);
      ttys.push(tty);
    }

    await writeYaml(CONFIG_FILE, {
      mappings: tty0ttys.map((tty0tty, i) => ({
        serial_port: tty0tty.tty1,
        baud_rate: 115200,
        tcp_port: TEST_PORT + i,
      })),
    });

    bridge = spawn('run');
  });

  afterEach(async () => {
    await bridge.end();

    await Promise.all(ttys.map(tty => tty.close()));
    await Promise.all(tty0ttys.map(tty0tty => tty0tty.end()));
  });

  async function testOneConnection(port: number, tty: Tty): Promise<void> {
    const client = new TcpClient();
    try {
      await tryConnect(client, { port });

      for (let i = 0; i < EXCHANGE_COUNT; i++) {
        await client.pushLine('hello1');
        expect(await tty.popLine()).to.eq('hello1');

        await tty.pushLine('hello2');
        expect(await client.popLine()).to.eq('hello2');
      }
    } finally {
      await client.close();
    }
  }

  itLinux64(`Can run ${FORWARDING_COUNT} streams in parallel`, async () => {
    await Promise.all(ttys.map((tty, i) => testOneConnection(TEST_PORT + i, tty)));
  });
});
