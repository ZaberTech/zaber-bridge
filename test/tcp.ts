import net from 'net';
import Bluebird from 'bluebird';
import { Readline } from './readline';

export class TcpClient {
  private socket: net.Socket;
  private readline: Readline;
  private endPromise: Bluebird.Resolver<void>;
  private error: any = null;
  private _closed = true;

  public get closed(): boolean {
    return this._closed;
  }

  public async connect(host: string, port: number): Promise<void> {
    this.error = null;
    const socket = net.createConnection(port, host);
    this.socket = socket;

    this.socket.once('close', hadErr => {
      if (this.socket !== socket) {
        return;
      }
      this._closed = true;

      if (this.endPromise) {
        if (hadErr) {
          this.endPromise.reject(new Error('Transmission error'));
        } else {
          this.endPromise.resolve();
        }
      }

      if (this.readline) {
        this.readline.close();
      }
    });
    this.socket.once('error', err => {
      if (this.socket !== socket) {
        return;
      }
      this.error = err;
    });

    await new Promise<void>((resolve, reject) => {
      this.socket.once('connect', resolve);
      this.socket.once('error', reject);
    });

    this._closed = false;
    this.socket.setNoDelay(true);
    this.socket.setKeepAlive(true);
    this.readline = new Readline(this.socket);
  }

  public popLine(): Promise<string> {
    if (this.error) {
      throw this.error;
    }
    return this.readline.popLine();
  }

  public async pushLine(line: string): Promise<void> {
    if (this.error) {
      throw this.error;
    }
    await Bluebird.fromCallback(cb => this.socket.write(line + '\n', cb));
  }

  public async close(): Promise<void> {
    await Bluebird.fromCallback(cb => this.socket.end(() => cb(null)));

    await this.waitForClosing();
  }

  public async waitForClosing(): Promise<void> {
    this.endPromise = Bluebird.defer();
    try {
      await this.endPromise;
    } finally {
      this.endPromise = null;
    }
  }
}
