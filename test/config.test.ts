import { expect } from 'chai';
import { promises as fs } from 'fs';

import { readYaml } from './yml';
import { run } from './run';
import { cleanConfig, CONFIG_FILE } from './utils';

describe('Config', () => {
  beforeEach(async () => {
    await cleanConfig();
  });
  afterEach(async () => {
    await cleanConfig();
  });

  describe('add', () => {
    it('adds port simple mapping', async () => {
      await run('config add --serial-port COM3 --tcp-port 12345');

      const config = await readYaml(CONFIG_FILE);
      expect(config).to.deep.eq({
        mappings: [
          {
            serial_port: 'COM3',
            baud_rate: 115200,
            tcp_port: 12345,
          }
        ]
      });
    });

    it('respects protocol', async () => {
      await run('config add --serial-port COM3 --tcp-port 12345 --protocol binary');

      const config = await readYaml(CONFIG_FILE);
      expect(config).to.deep.eq({
        mappings: [
          {
            serial_port: 'COM3',
            baud_rate: 9600,
            tcp_port: 12345,
          }
        ]
      });
    });

    it('allows baud rate override', async () => {
      await run('config add --serial-port COM3 --tcp-port 12345 --baud-rate 19200');

      const config = await readYaml(CONFIG_FILE);
      expect(config).to.deep.eq({
        mappings: [
          {
            serial_port: 'COM3',
            tcp_port: 12345,
            baud_rate: 19200,
          }
        ]
      });
    });

    it('sorts the config by serial port', async () => {
      await run('config add --serial-port COM4 --tcp-port 12345');
      await run('config add --serial-port COM2 --tcp-port 12346');
      await run('config add --serial-port COM3 --tcp-port 12348');
      await run('config add --serial-port COM3 --tcp-port 12347');

      const config = await readYaml(CONFIG_FILE);
      expect(config).to.deep.eq({
        mappings: [{
          serial_port: 'COM2',
          tcp_port: 12346,
          baud_rate: 115200,
        }, {
          serial_port: 'COM3',
          tcp_port: 12347,
          baud_rate: 115200,
        }, {
          serial_port: 'COM3',
          tcp_port: 12348,
          baud_rate: 115200,
        }, {
          serial_port: 'COM4',
          tcp_port: 12345,
          baud_rate: 115200,
        }]
      });
    });

    it('throws error when tcp ports are not unique', async () => {
      await run('config add --serial-port COM3 --tcp-port 12345');

      const promise = run('config add --serial-port COM4 --tcp-port 12345');
      await expect(promise).to.be.rejectedWith();

      const { stderr } = await promise.catch(e => e);

      expect(stderr).to.contain('Tcp port 12345 is used in the config more than once.');
    });
  });

  it('lists ports', async () => {
    await run('config add --serial-port COM2 --tcp-port 12345 --baud-rate 19200');
    await run('config add --serial-port COM3 --tcp-port 12346 --protocol binary');
    await run('config add --serial-port COM4 --tcp-port 12347 --protocol ascii');
    const { stdout } = await run('config list');

    expect(stdout.trim()).to.eq(`
+-------------+----------+----------------+
| Serial Port | TCP Port | Baud Rate      |
+-------------+----------+----------------+
| COM2        |    12345 |          19200 |
| COM3        |    12346 | (Binary)  9600 |
| COM4        |    12347 | (ASCII) 115200 |
+-------------+----------+----------------+
`.trim()
    );
  });

  describe('remove', () => {
    beforeEach(async () => {
      await run('config add --serial-port COM3 --tcp-port 12345');
      await run('config add --serial-port COM3 --tcp-port 12346');
      await run('config add --serial-port COM4 --tcp-port 12347');
    });

    it('removes ports by name (all of them)', async () => {
      await run('config remove --serial-port COM3');

      const config = await readYaml(CONFIG_FILE);
      expect(config).to.deep.eq({
        mappings: [{
          serial_port: 'COM4',
          tcp_port: 12347,
          baud_rate: 115200,
        }]
      });
    });

    it('removes ports by tcp port', async () => {
      await run('config remove --tcp-port 12346');

      const config = await readYaml(CONFIG_FILE);
      expect(config).to.deep.eq({
        mappings: [{
          serial_port: 'COM3',
          tcp_port: 12345,
          baud_rate: 115200,
        }, {
          serial_port: 'COM4',
          tcp_port: 12347,
          baud_rate: 115200,
        }]
      });
    });
  });

  it('prints warning when cannot parse file', async () => {
    await fs.writeFile(CONFIG_FILE, '!', 'utf8');

    const { stdout } = await run('config list');
    expect(stdout).to.contain('WARNING: Error when parsing config file');
  });
});
