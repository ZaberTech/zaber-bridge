import fs from 'fs';
import Bluebird from 'bluebird';
import childProcess, { ChildProcess } from 'child_process';

import { Readline } from './readline';

export class Tty {
  private wstream: fs.WriteStream;
  private readline: Readline;
  private readError: any;
  private writeError: any;
  private closed = false;
  private catProcess: ChildProcess;

  constructor(public readonly file: string) {
  }

  public async open(): Promise<void> {
    // We use cat process, because reading from tty directly blocks the node when there are multiple (>4) instances.
    // I have not found out why.
    this.catProcess = childProcess.spawn('cat', [this.file], {
      stdio: ['ignore', 'pipe', 'ignore'],
      shell: true,
    });
    this.catProcess.once('error', e => { this.readError = e; });

    this.readline = new Readline(this.catProcess.stdout);

    this.wstream = fs.createWriteStream(this.file);
    this.wstream.once('error', e => { this.writeError = e; });
  }

  public async popLine(): Promise<string> {
    if (this.readError) {
      throw this.readError;
    }

    return await this.readline.popLine();
  }

  public async pushLine(line: string): Promise<void> {
    if (this.writeError) {
      throw this.writeError;
    }

    await Bluebird.fromCallback(cb => this.wstream.write(line + '\n', cb));
  }

  public async close(): Promise<void> {
    if (this.closed) {
      return;
    }
    this.closed = true;

    this.readline.close();

    this.catProcess.kill();
    this.wstream.close();
  }
}
