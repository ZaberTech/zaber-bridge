import childProcess, { ChildProcess } from 'child_process';
import path from 'path';
import os from 'os';

type ClosePromise = Promise<{ stdout: string; stderr: string; }>;

export class Bridge {
  constructor(
    private readonly process: ChildProcess,
    private readonly endPromise: ClosePromise,
    ) { }

    public end(): ClosePromise {
      this.process.kill();

      return this.waitTillEnd();
    }

    public waitTillEnd(): ClosePromise {
      return this.endPromise;
    }
}

const archToDir: {
  [key: string]: string
} = {
  x64: 'amd64',
  ia32: '386',
  arm: 'arm',
  arm64: 'arm64',
};
const platformToDir: {
  [key: string]: string
} = {
  win32: 'windows',
  linux: 'linux',
  darwin: 'darwin',
};

export function spawn(args: string): Bridge {
  let file = path.join(__dirname, '..', 'bin', platformToDir[os.platform()], archToDir[os.arch()], 'zaber-bridge');
  if (os.platform() === 'win32') {
    file += '.exe';
  }

  let newProcess: ChildProcess;
  const promise: ClosePromise = new Promise((resolve, reject) => {
    newProcess = childProcess.execFile(file, args.split(' '), {
      cwd: __dirname,
      env: {
        ...process.env,
        ZABER_BRIDGE_CONFIG_FILE: path.join(__dirname, 'zaber-bridge-config.yml'),
      }
    }, (error, stdout, stderr) => {
      if (error) {
        return reject({ error, stdout, stderr });
      }
      resolve({ stdout, stderr });
    });
  });

  return new Bridge(newProcess, promise);
}

export function run(args: string): ClosePromise {
  const bridge = spawn(args);
  return bridge.waitTillEnd();
}
