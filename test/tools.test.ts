import { expect } from 'chai';
import { run } from './run';
import os from 'os';

describe('tools', () => {
  describe('list', () => {
    it('displays list of serial ports', async () => {
      const { stdout } = await run('tools list');

      expect(stdout).to.contain('ports');
    });
  });

  describe('ip', () => {
    it('displays list of IP addresses', async () => {
      const { stdout } = await run('tools ip');

      expect(stdout).to.contain('IP address');
    });
  });

  describe('hostname', () => {
    it('displays hostname', async () => {
      const { stdout } = await run('tools hostname');

      expect(stdout).to.contain(`Hostname: ${os.hostname()}`);
    });
  });
});
