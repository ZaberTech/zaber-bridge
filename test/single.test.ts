import { expect } from 'chai';

import { createTty0Tty, Tty0Tty } from './tty0tty';
import { TEST_PORT, itLinux64, tryConnect } from './utils';
import { Tty } from './tty';
import { spawn, Bridge} from './run';
import { TcpClient } from './tcp';

describe('Single', () => {
  let tty: Tty;
  let tty0tty: Tty0Tty;

  beforeEach(async () => {
    tty0tty = await createTty0Tty();

    tty = new Tty(tty0tty.tty2);
    await tty.open();
  });
  afterEach(async () => {
    await tty.close();
    await tty0tty.end();
  });

  describe('single opens single mapping', () => {
    let bridge: Bridge;

    afterEach(async () => {
      await bridge.end();
    });

    itLinux64('communicates both ways', async () => {
      bridge = spawn(`single -s ${tty0tty.tty1} -p ${TEST_PORT}`);

      const client = new TcpClient();
      try {
        await tryConnect(client);

        await client.pushLine('hello1');
        expect(await tty.popLine()).to.eq('hello1');

        await tty.pushLine('hello2');
        expect(await client.popLine()).to.eq('hello2');
      } finally {
        await client.close();
      }

    });
  });
});
