import { expect } from 'chai';

import { createTty0Tty } from './tty0tty';
import { Tty } from './tty';
import { itLinux64 } from './utils';

describe('Tty0Tty', () => {
  itLinux64('works on this computer', async () => {
    const tty0tty = await createTty0Tty();

    const tty1 = new Tty(tty0tty.tty1);
    const tty2 = new Tty(tty0tty.tty2);

    await tty1.open();
    await tty2.open();

    await tty1.pushLine('hello1');
    expect(await tty2.popLine()).to.eq('hello1');

    await tty2.pushLine('hello2');
    expect(await tty1.popLine()).to.eq('hello2');

    await tty1.close();
    await tty2.close();

    await tty0tty.end();
  });
});
