package main

import (
	_ "gitlab.izaber.com/software-public/zaber-bridge/internal/init"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/ioc"
)

func main() {
	ioc.Instance.GetApp().Run()
}
