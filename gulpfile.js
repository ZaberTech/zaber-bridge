/* eslint-disable camelcase,no-console */
const child_process = require('child_process');
const { promisify } = require('bluebird');
const os = require('os');
const package = require('./package.json');
const path = require('path');
const argv = require('yargs').argv;
const replace = require('replace-in-file');
const glob = promisify(require('glob'));
const { series, parallel } = require('gulp');

if (!process.env.GOPATH) {
  console.log('set GOPATH');
  process.exit(1);
}

const gen_license = () => require('./licensing/generate').collectLicenses();

const exec = (...args) => new Promise((resolve, reject) => {
  const childProcess = child_process.exec(...args, err => {
    if (err) {
      return reject(err);
    }

    resolve();
  });
  childProcess.stdout.pipe(process.stdout);
  childProcess.stderr.pipe(process.stderr);
});

const lint_ts = () => exec('npx tslint "test/**/*.ts"');
const lint_js = () => exec('npx eslint "*.js"');
const lint = series(lint_js, lint_ts);

const extensions = {
  win32: '.exe',
};

const build_template = async (GOOS, GOARCH) => {
  const extension = extensions[os.platform()] || '';

  await exec(`go build -o bin/${GOOS}/${GOARCH}/zaber-bridge${extension} cmd/main.go`, {
    env: {
      GOOS,
      GOARCH,
      ...process.env,
    }
  });
};

const build_linux_386 = () => build_template('linux', '386');
const build_linux_amd64 = () => build_template('linux', 'amd64');
const build_linux_arm = () => build_template('linux', 'arm');
const build_linux_arm64 = () => build_template('linux', 'arm64');
const build_linux = series(build_linux_386, build_linux_amd64);

const build_windows_386 = () => build_template('windows', '386');
const build_windows_amd64 = () => build_template('windows', 'amd64');
const build_windows_arm64 = () => build_template('windows', 'arm64');
const build_windows = async () => {
  const arch = os.arch();
  switch (arch) {
  case 'arm64':
    await build_windows_arm64();
    break;
  default:
    await build_windows_386();
    await build_windows_amd64();
    break;
  }
};

const build_darwin_amd64 = () => build_template('darwin', 'amd64');
const build_darwin_arm64 = () => build_template('darwin', 'arm64');
const build_darwin = series(build_darwin_amd64, build_darwin_arm64);

const build = (() => {
  const platform = os.platform();
  const arch = os.arch();
  switch (platform) {
  case 'win32':
    return build_windows;
  case 'linux':
    switch (arch) {
    case 'arm':
      return build_linux_arm;
    case 'arm64':
      return build_linux_arm64;
    default:
      return build_linux;
    }
  case 'darwin':
    switch(arch) {
    case 'i386':
      return build_darwin_amd64;
    case 'arm64':
      return build_darwin_arm64;
    default:
      return build_darwin;
    }
  default:
    throw new Error('Unsupported OS');
  }
})();

const test_unit = () => exec('npm run test:unit');
const test_integration = () => exec('npm run test:integration');
const test = series(test_unit, test_integration);

const all = series(lint, build, test);

const set_version_go = async () => {
  await replace({
    files: path.join(__dirname, 'internal', 'app', 'version.go'),
    from: /const version = ".*"/g,
    to: `const version = "${argv.new_version}"`,
  });
};
const set_version_global = async () => {
  await exec(`npm --no-git-tag-version version "${argv.new_version}"`);
};

const set_version = parallel(set_version_global, set_version_go);

const zip_binaries = async () => {
  const binaries = await glob(path.join('bin', '**', 'zaber-bridge*'));
  for (const binary of binaries) {
    const dir = path.dirname(binary);
    const dist = dir.replace('bin', 'dist');
    await exec([
      `mkdir -p ${dist}`,
      `cd ${dir}`,
      'cp ../../../licensing/LICENSE.txt .',
      `zip ../../../${dist}/zaber-bridge.zip ${path.basename(binary)} LICENSE.txt`,
    ].join(' && '));
  }
};

const publish = async () => {
  await zip_binaries();

  const toFolder = `downloads/bridge/${package.version}/`;
  await exec(`aws s3 cp dist/ ${process.env.BUCKET}/${toFolder} --recursive`);
};

module.exports = {
  lint_js,
  lint_ts,
  lint,
  gen_license,
  build,
  test_unit,
  test_integration,
  test,
  set_version,
  zip_binaries,
  publish,
  all,
};
