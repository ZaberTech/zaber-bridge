package out

import (
	"fmt"
	"os"
)

type Logger interface {
	Error(v ...interface{}) error
	Warning(v ...interface{}) error
	Info(v ...interface{}) error
}

var logger Logger

func SetLogger(newLogger Logger) {
	logger = newLogger
}

func Print(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)

	if l := logger; l != nil {
		_ = l.Info(msg)
	} else {
		fmt.Println(msg)
	}
}

func Warn(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)

	if l := logger; l != nil {
		_ = l.Warning(msg)
	} else {
		fmt.Println("WARNING: " + msg)
	}
}

func Fatal(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)

	if l := logger; l != nil {
		_ = l.Error(msg)
	} else {
		fmt.Fprintln(os.Stderr, msg)
	}

	os.Exit(1)
}

func Fatale(err error) {
	Fatal("Error: %s", err.Error())
}
