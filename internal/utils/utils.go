package utils

import (
	"os"
)

func GetCurrentWorkingDirectory() string {
	if dir, err := os.Getwd(); err == nil {
		return dir
	}
	return ""
}
