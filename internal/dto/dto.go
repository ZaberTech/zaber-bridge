package dto

type PortMapping struct {
	SerialPort string `yaml:"serial_port"`
	TCPPort    uint   `yaml:"tcp_port"`
	BaudRate   uint   `yaml:"baud_rate,omitempty"`
}

type ConfigRoot struct {
	Mappings []*PortMapping `yaml:"mappings"`
}
