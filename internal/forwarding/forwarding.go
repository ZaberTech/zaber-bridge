package forwarding

import (
	"fmt"
	"net"
	"sync"

	"gitlab.izaber.com/software-public/zaber-bridge/internal/dto"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/out"
)

type Forwarding struct {
	config           *dto.PortMapping
	listener         net.Listener
	activeConnection *forwardingConnection
	closed           bool
	lock             sync.Mutex
	done             chan bool
}

func NewForwarding(config dto.PortMapping) *Forwarding {
	return &Forwarding{
		config: &config,
		done:   make(chan bool, 1),
	}
}

func (m *Forwarding) Start() error {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", m.config.TCPPort))
	if err != nil {
		return fmt.Errorf("Cannot start listening: %s", err.Error())
	}

	m.listener = listener

	go m.listeningLoop()

	return nil
}

func (m *Forwarding) listeningLoop() {
	for {
		connection, err := m.listener.Accept()
		if err != nil {
			if m.IsClosed() {
				break
			}
			out.Warn("Cannot accept connection: %s", err.Error())
			continue
		}

		newConnection := newForwardingConnection(connection, m.config)

		m.setConnection(newConnection)
	}

	m.setConnection(nil)
	m.done <- true
}

func (m *Forwarding) setConnection(newConnectionOrNil *forwardingConnection) {
	oldConnection := m.activeConnection
	m.activeConnection = newConnectionOrNil

	if oldConnection != nil {
		oldConnection.close()
	}
	if newConnectionOrNil != nil {
		newConnectionOrNil.start()
	}
}

func (m *Forwarding) IsClosed() bool {
	m.lock.Lock()
	defer m.lock.Unlock()

	return m.closed
}

func (m *Forwarding) Close() error {
	m.lock.Lock()
	wasClosed := m.closed
	m.closed = true
	m.lock.Unlock()

	if wasClosed || m.listener == nil {
		return nil
	}

	err := m.listener.Close()
	<-m.done

	m.listener = nil

	return err
}

func (m *Forwarding) String() string {
	return fmt.Sprintf("TCP %d <-> %s", m.config.TCPPort, m.config.SerialPort)
}
