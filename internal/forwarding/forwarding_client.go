package forwarding

import (
	"fmt"
	"io"
	"net"
	"sync"

	serial "github.com/zabertech/go-serial"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/dto"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/out"
)

const bufferSize = 8 * 1024

type forwardingConnection struct {
	config        *dto.PortMapping
	connection    net.Conn
	connectionStr string
	port          serial.Port
	closeLock     sync.Mutex
	closed        bool
}

func newForwardingConnection(connection net.Conn, config *dto.PortMapping) *forwardingConnection {
	return &forwardingConnection{
		config:        config,
		connection:    connection,
		connectionStr: connection.RemoteAddr().String(),
	}
}

func (m *forwardingConnection) start() {
	out.Print("Accepted connection: %s", m.String())

	mode := &serial.Mode{
		BaudRate: (int)(m.config.BaudRate),
	}
	port, err := serial.Open(m.config.SerialPort, mode)
	if err != nil {
		out.Warn("Cannot open serial port %s: %s", m.String(), err.Error())
		m.close()
		return
	}
	m.port = port

	connection := m.connection

	go m.readLoop(port, connection)
	go m.readLoop(connection, port)
}

func (m *forwardingConnection) readLoop(reader io.Reader, writer io.Writer) {
	buffer := make([]byte, bufferSize)

	_, err := io.CopyBuffer(writer, reader, buffer)
	if err != nil && !m.isClosed() {
		out.Warn("Error when forwarding %s: %s", m.String(), err.Error())
	}

	m.close()
}

func (m *forwardingConnection) isClosed() bool {
	m.closeLock.Lock()
	defer m.closeLock.Unlock()

	return m.closed
}

func (m *forwardingConnection) close() {
	m.closeLock.Lock()
	defer m.closeLock.Unlock()

	if m.closed {
		return
	}
	m.closed = true

	m.closeConnection()
	m.closePort()

	out.Print("Connection closed %s", m.String())
}

func (m *forwardingConnection) closeConnection() {
	if m.connection == nil {
		return
	}
	if err := m.connection.Close(); err != nil {
		out.Warn("Cannot close connection %s: %s", m.String(), err.Error())
	}
	m.connection = nil
}

func (m *forwardingConnection) closePort() {
	if m.port == nil {
		return
	}
	if err := m.port.Close(); err != nil {
		out.Warn("Cannot close serial port %s: %s", m.String(), err.Error())
	}
	m.port = nil
}

func (m *forwardingConnection) String() string {
	return fmt.Sprintf("%s <-> %s", m.connectionStr, m.config.SerialPort)
}
