package run

import (
	"gitlab.izaber.com/software-public/zaber-bridge/internal/cliutils"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/dto"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/forwarding"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/ioc"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/out"
	cli "gopkg.in/urfave/cli.v1"
)

type runImpl struct {
	config ioc.Config
	tools  ioc.Tools
}

func NewRun(app ioc.App, config ioc.Config, tools ioc.Tools) ioc.Run {
	run := &runImpl{
		config: config,
		tools:  tools,
	}

	app.AddCommand(&cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Runs the application to forward data between network and serial ports.",
		Action: func(c *cli.Context) error {
			return run.run(c)
		},
	})

	app.AddCommand(&cli.Command{
		Name:    "single",
		Aliases: []string{"s"},
		Usage:   "Runs a single specified mapping to forward data between network and serial ports.",
		Action: func(c *cli.Context) error {
			return run.single(c)
		},
		Flags: cliutils.CreateMappingFlags(),
	})

	return run
}

func (run *runImpl) run(_ *cli.Context) error {
	ctrl := run.CreateControl()
	ctrl.(*runControl).terminateOnSignal()

	run.Run(ctrl)

	return nil
}

func (run *runImpl) single(c *cli.Context) error {
	ctrl := run.CreateControl()
	ctrl.(*runControl).terminateOnSignal()

	singleMapping := cliutils.CreateMappingFromFlags(c)
	run.forwardMappings(ctrl, []*dto.PortMapping{singleMapping})

	return nil
}

func (run *runImpl) Run(control ioc.RunControl) {
	mappings := run.config.GetConfig().Mappings
	run.forwardMappings(control, mappings)
}

func (run *runImpl) forwardMappings(control ioc.RunControl, mappings []*dto.PortMapping) {
	out.Print("Starting...")

	ctrlImpl := control.(*runControl)

	if len(mappings) == 0 {
		out.Print("No serial port mappings active.")
	}

	forwardings := make([]*forwarding.Forwarding, len(mappings))
	for i, mapping := range mappings {
		forwardings[i] = forwarding.NewForwarding(*mapping)
	}

	for _, forwarding := range forwardings {
		if err := forwarding.Start(); err != nil {
			out.Fatal("Error when starting %s: %s", forwarding.String(), err.Error())
		}
		out.Print("Started %s", forwarding.String())
	}

	_ = run.tools.DisplayHostname()
	_ = run.tools.ListIPAddresses()

	out.Print("Press Ctrl+C to stop...")

	ctrlImpl.started.Done()
	<-ctrlImpl.termination

	out.Print("Stopping...")

	for _, forwarding := range forwardings {
		if err := forwarding.Close(); err != nil {
			out.Warn("Error while stopping %s: %s", forwarding.String(), err.Error())
		}
	}

	out.Print("Stopped")
	ctrlImpl.stopped.Done()
}
