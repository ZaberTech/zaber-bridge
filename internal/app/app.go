package app

import (
	"os"
	"sort"

	"gitlab.izaber.com/software-public/zaber-bridge/internal/ioc"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/out"
	cli "gopkg.in/urfave/cli.v1"
)

type appImpl struct {
	cliApp *cli.App
}

// following templates taken and modified from:
// go/pkg/mod/gopkg.in/urfave/cli.v1@v1.20.0/help.go

const appHelpTemplate = `NAME:
   {{.Name}}{{if .Usage}} - {{.Usage}}{{end}}

USAGE:
   {{if .UsageText}}{{.UsageText}}{{else}}{{.HelpName}} {{if .VisibleFlags}}[global options]{{end}}{{if .Commands}} command [command options]{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}[arguments...]{{end}}{{end}}{{if .Version}}{{if not .HideVersion}}

VERSION:
   {{.Version}}{{end}}{{end}}{{if .Description}}

DESCRIPTION:
   {{.Description}}{{end}}{{if len .Authors}}

AUTHOR{{with $length := len .Authors}}{{if ne 1 $length}}S{{end}}{{end}}:
   {{range $index, $author := .Authors}}{{if $index}}
   {{end}}{{$author}}{{end}}{{end}}{{if .VisibleCommands}}

COMMANDS:{{range .VisibleCategories}}{{if .Name}}
   {{.Name}}:{{end}}{{range .VisibleCommands}}
   {{join .Names ", "}}{{"\t"}}{{.Usage}}{{end}}{{end}}{{end}}{{if .VisibleFlags}}

GLOBAL OPTIONS:
   {{range $index, $option := .VisibleFlags}}{{if $index}}
   {{end}}{{$option}}{{end}}{{end}}{{if .Copyright}}

COPYRIGHT:
   {{.Copyright}}{{end}}

REPOSITORY:
   https://gitlab.com/ZaberTech/zaber-bridge
`

const commandHelpTemplate = `NAME:
   {{.HelpName}} - {{.Usage}}

USAGE:
   {{if .UsageText}}{{.UsageText}}{{else}}{{.HelpName}}{{if .VisibleFlags}} [command options]{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}[arguments...]{{end}}{{end}}{{if .Category}}

CATEGORY:
   {{.Category}}{{end}}{{if .Description}}

DESCRIPTION:
   {{.Description}}{{end}}{{if .VisibleFlags}}

OPTIONS:
   {{range .VisibleFlags}}{{.}}
   {{end}}{{end}}
`

const subcommandHelpTemplate = `NAME:
   {{.HelpName}} - {{if .Description}}{{.Description}}{{else}}{{.Usage}}{{end}}

USAGE:
   {{if .UsageText}}{{.UsageText}}{{else}}{{.HelpName}} command{{if .VisibleFlags}} [command options]{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}[arguments...]{{end}}{{end}}

COMMANDS:{{range .VisibleCategories}}{{if .Name}}
   {{.Name}}:{{end}}{{range .VisibleCommands}}
   {{join .Names ", "}}{{"\t"}}{{.Usage}}{{end}}
{{end}}{{if .VisibleFlags}}
OPTIONS:
   {{range .VisibleFlags}}{{.}}
   {{end}}{{end}}
`

func NewApp() ioc.App {
	app := cli.NewApp()

	cli.AppHelpTemplate = appHelpTemplate
	cli.CommandHelpTemplate = commandHelpTemplate
	cli.SubcommandHelpTemplate = subcommandHelpTemplate

	app.Name = "zaber-bridge"
	app.Version = version
	app.Authors = []cli.Author{
		cli.Author{
			Name:  "Zaber Software Team",
			Email: "contact@zaber.com",
		},
	}
	app.Copyright = "(c) 2019 Zaber Technologies Inc."
	app.Usage = "Bridges serial ports and network exposing serial ports as TCP/IP ports"

	return &appImpl{
		cliApp: app,
	}
}

func (a *appImpl) AddCommand(cmd *cli.Command) {
	a.cliApp.Commands = append(a.cliApp.Commands, *cmd)
}
func (a *appImpl) AddFlag(flag cli.Flag) {
	a.cliApp.Flags = append(a.cliApp.Flags, flag)
}

func (a *appImpl) Run() {
	app := a.cliApp

	sort.Sort(cli.CommandsByName(app.Commands))

	err := app.Run(os.Args)
	if err != nil {
		out.Fatale(err)
	}
}
