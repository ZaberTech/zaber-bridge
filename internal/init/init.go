package init

import (
	"gitlab.izaber.com/software-public/zaber-bridge/internal/app"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/config"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/ioc"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/run"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/service"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/tools"
)

type container struct {
	app     ioc.App
	config  ioc.Config
	run     ioc.Run
	tools   ioc.Tools
	service ioc.Service
}

func init() {
	container := &container{}

	container.app = app.NewApp()
	container.config = config.NewConfig(container.app)
	container.tools = tools.NewTools(container.app)
	container.run = run.NewRun(container.app, container.config, container.tools)
	container.service = service.NewService(container.app, container.run, container.config)

	ioc.Instance = container
}

func (container *container) GetApp() ioc.App {
	return container.app
}
