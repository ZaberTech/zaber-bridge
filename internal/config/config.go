package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"sync"

	texttable "github.com/syohex/go-texttable"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/cliutils"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/constants"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/dto"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/ioc"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/out"
	cli "gopkg.in/urfave/cli.v1"
	yaml "gopkg.in/yaml.v2"
)

const configFilePathEnv = "ZABER_BRIDGE_CONFIG_FILE"

type configImpl struct {
	configFile string
	root       *dto.ConfigRoot
	lock       sync.Mutex
}

type flags struct {
	serialPort string
	tcpPort    uint
}

var specifiedFlags flags

func NewConfig(app ioc.App) ioc.Config {
	config := &configImpl{
		configFile: getConfigFilePath(),
	}

	app.AddCommand(&cli.Command{
		Name:    "config",
		Aliases: []string{"cfg"},
		Usage:   "Configure serial port mapping",
		Before: func(_ *cli.Context) error {
			config.readConfig(false)
			return nil
		},
		Subcommands: []cli.Command{
			{
				Name:  "add",
				Usage: "Adds new serial port mapping",
				Action: func(c *cli.Context) error {
					return config.add(c)
				},
				Flags: cliutils.CreateMappingFlags(),
			},
			{
				Name:  "list",
				Usage: "Lists serial port mappings",
				Action: func(c *cli.Context) error {
					return config.list(c)
				},
			},
			{
				Name:  "remove",
				Usage: "Removes serial port mapping",
				Action: func(c *cli.Context) error {
					return config.remove(c)
				},
				Flags: []cli.Flag{
					cli.StringFlag{
						Name:        "serial-port, s",
						Usage:       "Specifies the mapped serial port to be removed.",
						Destination: &specifiedFlags.serialPort,
					},
					cli.UintFlag{
						Name:        "tcp-port, p",
						Usage:       "Specifies the mapped TCP port to be removed.",
						Destination: &specifiedFlags.tcpPort,
					},
				},
			},
		},
	})

	return config
}

func (config *configImpl) add(c *cli.Context) error {
	newMapping := cliutils.CreateMappingFromFlags(c)

	config.root.Mappings = append(config.root.Mappings, newMapping)

	config.validateAndSortConfig()

	config.writeConfig()

	config.printMapping()

	out.Print("New mapping added.")

	return nil
}

func (config *configImpl) list(_ *cli.Context) error {
	config.printMapping()

	return nil
}

func (config *configImpl) remove(_ *cli.Context) error {

	var filtered []*dto.PortMapping
	removed := 0

	for _, mapping := range config.root.Mappings {
		if mapping.SerialPort == specifiedFlags.serialPort || mapping.TCPPort == specifiedFlags.tcpPort {
			removed++
		} else {
			filtered = append(filtered, mapping)
		}
	}

	config.root.Mappings = filtered

	config.validateAndSortConfig()

	config.writeConfig()

	config.printMapping()

	out.Print("Removed %d mappings.", removed)

	return nil
}

func (config *configImpl) validateAndSortConfig() {
	uniquePort := make(map[uint]bool)

	mappings := config.root.Mappings

	for _, mapping := range mappings {
		if _, found := uniquePort[mapping.TCPPort]; found {
			out.Fatal("Tcp port %d is used in the config more than once.", mapping.TCPPort)
		}
		uniquePort[mapping.TCPPort] = true
	}

	sort.Slice(mappings, func(i, j int) bool {
		if mappings[i].SerialPort != mappings[j].SerialPort {
			return mappings[i].SerialPort < mappings[j].SerialPort
		}
		return mappings[i].TCPPort < mappings[j].TCPPort
	})
}

func (config *configImpl) printMapping() {
	if len(config.root.Mappings) == 0 {
		out.Print("No mappings configured.")
		return
	}

	table := &texttable.TextTable{}
	table.SetHeader("Serial Port", "TCP Port", "Baud Rate")

	for _, mapping := range config.root.Mappings {
		baudRate := fmt.Sprintf("%d", mapping.BaudRate)
		switch mapping.BaudRate {
		case constants.BaudRateASCII:
			baudRate = "(ASCII) " + baudRate
		case constants.BaudRateBinary:
			baudRate = "(Binary)  " + baudRate
		}
		table.AddRow(mapping.SerialPort, fmt.Sprintf("%d", mapping.TCPPort), baudRate)
	}

	out.Print(table.Draw())
}

func (config *configImpl) readConfig(warnOnNotExist bool) {
	config.root = &dto.ConfigRoot{}

	data, err := ioutil.ReadFile(config.configFile)
	if err != nil {
		if os.IsNotExist(err) {
			if warnOnNotExist {
				out.Warn("Config file does not exist (%s)", config.configFile)
			}
		} else {
			out.Warn("Error when reading config file %s: %s", config.configFile, err.Error())
		}
		return
	}

	var root dto.ConfigRoot
	err = yaml.Unmarshal(data, &root)
	if err != nil {
		out.Warn("Error when parsing config file %s: %s", config.configFile, err.Error())
		return
	}
	config.root = &root
}

func (config *configImpl) writeConfig() {
	data, err := yaml.Marshal(config.root)
	if err != nil {
		out.Fatal("Cannot serialize config: %s", err.Error())
	}

	directory := filepath.Dir(config.configFile)
	if err := os.MkdirAll(directory, 0700); err != nil {
		out.Fatal("Cannot create directories for config to file %s: %s", config.configFile, err.Error())
	}

	if err := ioutil.WriteFile(config.configFile, data, 0600); err != nil {
		out.Fatal("Cannot write config to file %s: %s", config.configFile, err.Error())
	}
}

func (config *configImpl) GetConfig() *dto.ConfigRoot {
	config.lock.Lock()
	defer config.lock.Unlock()

	if config.root == nil {
		config.readConfig(true)
	}

	return config.root
}

func (config *configImpl) GetDefaultFile() string {
	return getConfigFilePath()
}

func (config *configImpl) SetFile(configFile string) {
	config.lock.Lock()
	defer config.lock.Unlock()

	if config.root != nil {
		panic(fmt.Errorf("Settings already loaded"))
	}

	config.configFile = configFile
}
