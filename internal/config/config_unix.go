// +build !windows

package config

import (
	"os"
	"os/user"
	"path/filepath"

	"gitlab.izaber.com/software-public/zaber-bridge/internal/utils"
)

const configFileName = "config.yml"

func getHomeDirUnix() string {
	if currentUser, err := user.Current(); err == nil {
		return currentUser.HomeDir
	} else {
		return ""
	}
}

func getConfigFilePath() string {
	if envPath := os.Getenv(configFilePathEnv); envPath != "" {
		return envPath
	} else if os.Getuid() == 0 {
		return filepath.Join("/etc/zaber-bridge", configFileName)
	} else if homeDir := getHomeDirUnix(); homeDir != "" {
		return filepath.Join(homeDir, ".zaber-bridge", configFileName)
	} else if exeFile, err := os.Executable(); err == nil {
		return filepath.Join(filepath.Dir(exeFile), configFileName)
	} else if currentDir := utils.GetCurrentWorkingDirectory(); currentDir != "" {
		return filepath.Join(currentDir, configFileName)
	}
	return configFileName
}
