// +build windows

package config

import (
	"os"
	"path/filepath"

	"gitlab.izaber.com/software-public/zaber-bridge/internal/utils"
)

const configFileName = "zaber-bridge-config.yml"

func getConfigFilePath() string {
	if envPath := os.Getenv(configFilePathEnv); envPath != "" {
		return envPath
	} else if exeFile, err := os.Executable(); err == nil {
		return filepath.Join(filepath.Dir(exeFile), configFileName)
	} else if currentDir := utils.GetCurrentWorkingDirectory(); currentDir != "" {
		return filepath.Join(currentDir, configFileName)
	}
	return configFileName
}
