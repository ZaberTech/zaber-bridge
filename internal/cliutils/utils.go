package cliutils

import (
	"strings"

	"gitlab.izaber.com/software-public/zaber-bridge/internal/constants"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/dto"
	"gitlab.izaber.com/software-public/zaber-bridge/internal/out"
	cli "gopkg.in/urfave/cli.v1"
)

func CreateMappingFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:  "serial-port, s",
			Usage: "Specifies the mapped serial port. Required.",
		},
		cli.UintFlag{
			Name:  "tcp-port, p",
			Usage: "Specifies the mapped TCP port. Required.",
		},
		cli.StringFlag{
			Name:  "protocol, l",
			Usage: "Optionally specifies Zaber protocol (ASCII or Binary). This option determines the baud rate.",
			Value: "ascii",
		},
		cli.UintFlag{
			Name:  "baud-rate, b",
			Usage: "Optionally specifies the baud rate used when comunicating on the serial port.",
			Value: constants.BaudRateASCII,
		},
	}
}

func CreateMappingFromFlags(c *cli.Context) *dto.PortMapping {
	newMapping := &dto.PortMapping{
		SerialPort: c.String("serial-port"),
		TCPPort:    c.Uint("tcp-port"),
		BaudRate:   c.Uint("baud-rate"),
	}

	if len(newMapping.SerialPort) == 0 {
		out.Fatal("--serial-port not specified.")
	}
	if newMapping.TCPPort == 0 {
		out.Fatal("--tcp-port not specified.")
	}

	if c.IsSet("protocol") {
		if c.IsSet("baud-rate") {
			out.Fatal("Options --protocol and --baud-rate cannot be specified at the same time.")
		}

		protocol := c.String("protocol")

		switch strings.ToLower(protocol) {
		case "ascii":
			newMapping.BaudRate = constants.BaudRateASCII
		case "binary":
			newMapping.BaudRate = constants.BaudRateBinary
		default:
			out.Fatal("Unknown protocol %s.", protocol)
		}
	}

	return newMapping
}
