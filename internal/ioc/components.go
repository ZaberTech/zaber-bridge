package ioc

import (
	"gitlab.izaber.com/software-public/zaber-bridge/internal/dto"
	cli "gopkg.in/urfave/cli.v1"
)

type App interface {
	AddCommand(cmd *cli.Command)
	AddFlag(flag cli.Flag)
	Run()
}

type Config interface {
	GetConfig() *dto.ConfigRoot
	GetDefaultFile() string
	SetFile(configFile string)
}

type RunControl interface {
	Stop()
	WaitForStart()
	WaitForStop()
}

type Run interface {
	Run(control RunControl)
	CreateControl() RunControl
}

type Tools interface {
	ListPorts() error
	ListIPAddresses() error
	DisplayHostname() error
}

type Service interface {
}
