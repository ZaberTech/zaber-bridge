package ioc

type Container interface {
	GetApp() App
}

var Instance Container
