## Tool Chain Setup

* Install the [Go](https://golang.org/) language tools. You need at least
  version 1.11, which means the default Linux package may not be new enough.
* (Linux) Run `sudo apt-get install libc6-dev-i386 gcc-multilib g++-multilib` to enable 32-bit builds
  on a 64-bit install.
* Install Node.js (LTS)

## Environment Setup

* (Windows) You must have long path support enabled. How to do this varies
  between Windows 7 and 10.  Note that it only enables long path support for
  applications that are long path aware and Windows 7's Explorer is not one
  of them, but the command shell is.
* Create a directory somewhere to be the root of your Go workspace. In there,
  create a subdirectory named `src`.
  * Check out this repository under `src`. It functions as a Go package,
    and `src` is the location required by Go.
  * (Windows) To keep paths shorter, put it near the root directory of your
     disk, or use the `subst` command to map a drive letter to your workspace.
     Subst also makes an easy way to switch between Go workspaces without
     having to change GOPATH.
* Set the environment variable GOPATH to point to your workspace and
  add $GOPATH/bin to your PATH. You might want to use a shell script to set
  up environment variables if you are using Go for other projects, as it's
  easier to change this way than using the system properties dialog.
* Set the environment variable GO111MODULE to `on`.
* Change directory to src/zaber-bridge
* Run `npm install`

## Compiling

In the zaber-bridge directory:

* Run `npx gulp build` to build the binaries
* Run `npx gulp test` to run tests
* Run `npx gulp all` to run build and tests

## Release

In the zaber-bridge directory:

* Create branch release-X.X.X (where X.X.X stands for the released version)
* Prepare the release notes and add them to `CHANGELOG.md`
* Run `npx gulp set_version --new_version X.X.X` to set version for the library
* Commit, push
* You may download artifacts and test the binaries manually
* Execute manual job called publish
* Post the release notes to the `Zaber Bridge` thread in the `Software Updates` forum
* Create a tag named `vX.X.X` (substitute actual version) at the commit you published from and push the tag
