[:arrow_left: Index](README.md)

# Install Zaber Bridge on Windows

## Download

Download the zipped program binary from the link below depending on your platform:

* [:floppy_disk: amd64](https://www.zaber.com/support/software-downloads.php?product=bridge_win64&version=latest) (Most current-generation computers)
* [:floppy_disk: arm64](https://www.zaber.com/support/software-downloads.php?product=bridge_winarm64&version=latest)

## Install

Create a folder on your drive (e.g. `C:\zaber-bridge`) and extract the zip content into the folder.

In the folder click _File_ and _Open Windows PowerShell_. In the console run the following to verify the installation:

```
.\zaber-bridge.exe --help
```

Make sure to put `.\` in front of all example commands.

## Update

In order to update to the new version, reinstall the program following the installation instructions above.
