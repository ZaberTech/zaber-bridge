[:arrow_left: Index](README.md)

# Install Zaber Bridge on Linux

## Download

Download the zipped program binary from the link below depending on your platform:

* [:floppy_disk: amd64](https://www.zaber.com/support/software-downloads.php?product=bridge_linux64&version=latest) (Most current-generation computers)
* [:floppy_disk: arm64](https://www.zaber.com/support/software-downloads.php?product=bridge_linuxarm64&version=latest) (Raspberry PI and similar)
* [:floppy_disk: arm](https://www.zaber.com/support/software-downloads.php?product=bridge_linuxarm&version=latest) (Older Raspberry PI and similar)
* [:floppy_disk: x86](https://www.zaber.com/support/software-downloads.php?product=bridge_linux32&version=latest) (32-bit system)

## Install

Open a terminal in the download folder and extract the zip archive:

```bash
sudo unzip zaber-bridge.zip zaber-bridge -d /usr/local/bin/
```

Give binary permission to execute:

```bash
sudo chmod +x /usr/local/bin/zaber-bridge
```

Verify the installation by executing:

```bash
zaber-bridge --help
```

## Update

In order to update to the new version, reinstall the program following the installation instructions above.
