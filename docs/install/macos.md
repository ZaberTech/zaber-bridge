[:arrow_left: Index](README.md)

# Install Zaber Bridge on Mac OS

## Download

Download the zipped program binary from
[:floppy_disk: arm64](https://www.zaber.com/support/software-downloads.php?product=bridge_darwinarm64&version=latest)
[:floppy_disk: amd64](https://www.zaber.com/support/software-downloads.php?product=bridge_darwin64&version=latest) (Older Intel-based)

## Install

Open a terminal in the download folder and extract the zip archive:

```bash
sudo unzip zaber-bridge.zip zaber-bridge -d /usr/local/bin/
```

Give binary permission to execute:

```bash
sudo chmod +x /usr/local/bin/zaber-bridge
```

Verify the installation by executing:

```bash
zaber-bridge --help
```

If you have Mac OS Catalina or higher, you may encounter a dialog preventing you from running the application:

![Gatekeeper dialog](docs/img/mac-gatekeeper-dialog.png)

We are currently not planning to undergo the Apple certification process for Zaber Bridge.
If you wish to run the application, follow the video below to override security for Zaber Bridge:

![How to override security for zaber-bridge](docs/img/mac-gatekeeper.mp4)

## Update

In order to update to the new version, reinstall the program following the installation instructions above.
