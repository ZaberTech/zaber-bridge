[:arrow_left: Index](README.md)

# Running Zaber Bridge as a Service on Windows

In the folder where the program is installed, click _File_ and point to _Open Windows PowerShell_.
On the right side menu, click on _Open Windows PowerShell as administrator_.
To install the program as a service, enter the following command.


```bash
.\zaber-bridge.exe service install
```

After installation, control the service with  commands like `start, stop, restart`.
For example, to start the service enter:

```bash
.\zaber-bridge.exe service start
```

Remember to restart the service every time you change the configuration.

Another way to control the service is through the _Services_ application.
To launch the application, open the windows menu and enter _Services_ into the search.

![Windows Services](docs/img/services.png)

## Logs

If you wish to monitor the output of the service, you can do so in the _Event Viewer_ application.
To open the application, search for it in the windows menu.
To see the messages click _Windows Logs_ and then _Application_ in the left menu.
Messages from the service have _zaber-bridge_ in _Source_ column.

![Event Viewer](docs/img/event-viewer.png)
