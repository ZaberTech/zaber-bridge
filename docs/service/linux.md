[:arrow_left: Index](README.md)

# Running Zaber Bridge as a Service on Linux

Open a console window and enter the following to install the service:

```bash
sudo zaber-bridge service install --user $USER
```

After installation control the service with commands like `start, stop, restart`:

```bash
sudo zaber-bridge service start
```

Remember to restart the service every time you change the configuration.

## Logs

To view the service logs enter the following command:

```bash
journalctl -u zaber-bridge.service
```

The service logs use _syslog_ (if available).
If the above command does not work, use tools specific to your distribution to view the log.
