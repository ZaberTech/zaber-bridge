[:arrow_left: Index](README.md)

# Running Zaber Bridge as a Service on Mac OS

Open a console window and enter the following to install the service:

```bash
zaber-bridge service install
```

After installation control the service with commands like `start, stop, restart`:

```bash
zaber-bridge service start
```

Remember to restart the service every time you change the configuration.

## Logs

The service logs use _syslog_.
By Mac OS default settings, only warning and error messages are printed to `/var/log/system.log`.
The standard output of the service is printed as info messages.
If you wish to see all messages from the service, change the _syslog_ configuration by adding the following line to `/etc/asl.conf`:

``
? [= Sender zaber-bridge] file system.log
``
